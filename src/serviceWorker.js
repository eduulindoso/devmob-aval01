const staticDevAdmin = "dev-admin-site-v1"
const assets = [
  "index.html",
  "js/sb-admin-2.js",
  "js/sb-admin-2.min.js",
  "css/sb-admin-2.css",
  "css/sb-admin-2.min.css",
  "img/undraw_posting_photo.svg",
  "img/undraw_profile_1.svg",
  "img/undraw_profile_2.svg",
  "img/undraw_profile_3.svg",
  "img/undraw_profile.svg",
  "img/undraw_rocket.svg",

]

self.addEventListener("install", installEvent) => {
  installEvent.waitUnti(
    caches.open(staticDevAdmin).then(cache => {
      cache.addAll(assets)
    })
  )
}

self.addEventListener("fetch", fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then((resp) => {
      return resp || fetch(fetchEvent.request).then((response) => {
        // cache = caches.open(staticDevGumi);
        return caches.open(staticDevGumi).then((cache) => {
          cache.put(fetchEvent.request, response.clone());
          return response;
        });
      });
    })
  );
});
